# check_model_bias

Assess the effectiveness of different prediction models

## usage:

`import bias`

`result=bias.check_bias(y_true,y_pred)`

`med_log_bias=result[1]`

`med_abs_err=result[3]`    

## discussion 
Not sure which is the most effective for getting points closer to the mark so may not be the most useful metric. 

%wins is better, however NN optimiser will not feed a third variable into cost, only y_true and y_pred

