#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 13:25:49 2018

Satellite model bias assessor
Two types of bias assessment

1. check_bias: two input (y_true,y_pred)
2. compare_bias: triple input (y_true,y_pred,y_nasa)
3. test_check_bias: Make sure the bias assessor is working fine
4. plot_linear_trend: A simple trend function I made for plotting. 

@author: npittman
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress

def plot_linear_trend(x,y,title,xlab,ylab,diagonal_line=1,printer=1,logspace=0,color=np.ndarray(0)):
    """"
    Taken from npittmans package Scifunx
    Designed for chlorophyll trends <1mg/m3 (My Region, equatorial Pacific ocean.)
    
    Given an x and y (same size)
    + Title, X Label, Y Label
    
    Optional: Diagonal_line=0, default on.
    Optional: Printer=0, default on
    
    Returns linear statistics. 
    """
    
    x=np.ravel(x)
    y=np.ravel(y)
    mask=~np.isnan(x)
    x=x[mask]
    y=y[mask]
    
    slope, intercept, r_value, p_value, std_err = linregress(x,y)
    plt.figure(figsize=(10,10))
    mn=0
    mx=1
    x1=np.linspace(mn,mx,300)
    y1=slope*x1+intercept

    
    if color.size==0:
        plt.scatter(x,y,c='#061264'),plt.xlim([0,0.5]),plt.ylim([0,0.5])
    else:
        plt.scatter(x,y,c=color,cmap='coolwarm',vmin=0,vmax=0.5),plt.colorbar(),plt.xlim([0,0.6]),plt.ylim([0,0.6])
    
    if logspace==1:
        plt.gca().set_xscale('log'),plt.gca().set_yscale('log')
        plt.xlim([0,1]),plt.ylim([0,1])
    
    plt.title(title,fontsize=32),plt.xlabel(xlab,fontsize=28),plt.ylabel(ylab,fontsize=28)
    plt.tick_params(axis='both', which='major', labelsize=24)    #fontsize for ticks

    if diagonal_line==1:
        plt.plot(np.arange(0,1,0.001),np.arange(0,1,0.001),'--k')
    plt.plot(x1,y1,'-k',linewidth=3)  
    plt.savefig('../results/figures/'+title+'.png', format='png', dpi=300,transparent=True)   
    plt.show()
    if printer==1:
        print(title,'Slope: ',slope,' Intercept: ',intercept,' R2: ',r_value)
        
     
    return slope, intercept, r_value, p_value, std_err 


def check_bias(truth,model):
    '''will accept pd.dataframes/np.array() or single values
    Returns log bias, median log bias, mean absolute error and median absolute error.
    
        result=bias.check_bias(t,m)
        med_log_bias=result[1]
        med_abs_err=result[3]    
    '''

    try:
        bias=((model-truth)/truth)*100
        abs_error=abs(bias)
    except:
        #will break when a list is sent, numpy array and dataframes should work fine
        pass
        
    logbias=10**(np.nanmean(np.log10(model)-np.log10(truth)))

    medlogbias=10**(np.nanmedian(np.log10(model)-np.log10(truth))) 
    
    mae=10**(np.nanmean(abs(np.log10(model)-np.log10(truth))))
    
    med_ae=10**(np.nanmedian(abs(np.log10(model)-np.log10(truth))))

    return logbias,medlogbias,mae,med_ae
    
    
def compare_bias(truth,model_a,model_b):
    '''
    Given truth, model_a, model_b (nasa)
    returns bias statistics, %wins, MAE, etc
    '''
    a_bias=((model_a-truth)/truth)*100
    a_abs_error=abs(a_bias)
    
    b_bias=((model_b-truth)/truth)*100
    b_abs_error=abs(b_bias)
    
    a_wins,b_wins,draw=0,0,0
    try:
        for i in range(len(truth)):
            if a_abs_error.iloc[i]<b_abs_error.iloc[i]:
                a_wins+=1
            elif a_abs_error.iloc[i]>b_abs_error.iloc[i]:
                b_wins+=1
            else:
                draw+=1
    except:
        for i in range(len(truth)):
            if a_abs_error[i]<b_abs_error[i]:
                a_wins+=1
            elif a_abs_error[i]>b_abs_error[i]:
                b_wins+=1
            else:
                draw+=1
    total=a_wins+b_wins        
    #print(a_wins,b_wins,draw) 
    try:
        a_win_percent=(a_wins/total)*100
    except:
        print('nothing?')
        
    try:
        b_win_percent=(b_wins/total)*100
    except:
        print('nothing?')
        
    new_bias=10**(np.nanmean(np.log10(model_a)-np.log10(truth)))
    nasa_bias=10**(np.nanmean(np.log10(model_b)-np.log10(truth)))
    
    new_med_bias=10**(np.nanmedian(np.log10(model_a)-np.log10(truth)))
    nasa_med_bias=10**(np.nanmedian(np.log10(model_b)-np.log10(truth)))
    
    
    new_mae=10**(np.nanmean(abs(np.log10(model_a)-np.log10(truth))))
    nasa_mae=10**(np.nanmean(abs(np.log10(model_b)-np.log10(truth))))
        
    new_med_ae=10**(np.nanmedian(abs(np.log10(model_a)-np.log10(truth))))
    nasa_med_ae=10**(np.nanmedian(abs(np.log10(model_b)-np.log10(truth))))
    
    slope_b,int_b,r2_b,_,_=plot_linear_trend(truth,model_b,'NASA Algorithm vs Observed','In situ $mg/m^3$','NASA Chlor a')
    slope_a,int_a,r2_a,_,_=plot_linear_trend(truth,model_a,'New Algorithm vs Observed','In situ $mg/m^3$','New Algorithm Chlor a')
    
    print('NEW')   
    print('New was better:',np.round(a_win_percent,3),'% of observations')
    #print('A median bias: ',np.nanmedian(a_bias))
    #print('A mean bias: ',np.nanmean(a_bias))
    #print('A median error: ',np.nanmedian(a_abs_error))
    #print('A mean error: ',np.nanmean(a_abs_error))
    print('New Mean Log Bias: ',np.round(new_bias,3))
    print('New Median Log Bias: ',np.round(new_med_bias,3))
    print('New Mean AE: ',np.round(new_mae,3))
    print('New Median AE: ',np.round(new_med_ae,3))
    print('New Slope: ',np.round(slope_a,3))
    print('New Intercept: ',np.round(int_a,3))
    print('New R2: ', np.round(r2_a,3))
    
    print('')
    print('NASA')
    print('Nasa was better:',np.round(b_win_percent,3),'% of observations')
    #print('B median bias: ',np.nanmedian(b_bias))
    #print('B mean bias: ',np.nanmean(b_bias))
    #print('B median error: ',np.nanmedian(b_abs_error))
    #print('B mean error: ',np.nanmean(b_abs_error))
    print('Nasa Mean Log Bias: ',np.round(nasa_bias,3))
    print('Nasa Median Log Bias: ',np.round(nasa_med_bias,3))
    print('Nasa Mean AE: ',np.round(nasa_mae,3))
    print('Nasa Median AE: ',np.round(nasa_med_ae,3))
    print('Nasa Slope: ',np.round(slope_b,3))
    print('Nasa Intercept: ',np.round(int_b,3))
    print('Nasa R2: ', np.round(r2_b,3))
    
    #return a_bias,a_abs_error,b_bias,b_abs_error


def test_check_bias():
    t1=[np.array([5,4,3,2,1]),
        np.array([5,4,3,1,1])]
    
    output=check_bias(t1[0],t1[1])
    assert output==(0.8705505632961241, 1.0, 1.148698354997035, 1.0) #Something broke
    
    t2=[np.array([5,4,3,2,1]),
        np.array([5,4,3,2,1])]
    
    output=check_bias(t2[0],t2[1])
    assert output==(1.0, 1.0, 1.0, 1.0) #Something broke
    
    print('Seems to be working fine')
    


if __name__ == '__main__':
    test_check_bias()
    